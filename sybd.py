def run(branch, strings):
    answer = input("Ответ: ")
    branch = graph_traversal[branch][ANSWERS[answer]]
    print(strings[branch-1])
    return branch

ANSWERS = {"yes": 0, "no": 1}
graph_traversal = {1: (2, 3), 2: (4, 9), 3: (8, 14), 4: (23, 5), 5: (6, 10), 6: (13, 7), 7: (11, 18), 8: (12, 15), 9: (16, 6), 10: (17, 16), 11: (19, 18), 12: (21, 14), 13: (18, 19), 14: (21, 20), 15: (23, 22)}
with open('QUESTIONS.txt', 'r') as f:
    strings = [row.strip() for row in f]
    print("Ответом является yes или no\n", strings[0])
    branch = 1
    while branch in graph_traversal:
        branch = run(branch, strings)
    
