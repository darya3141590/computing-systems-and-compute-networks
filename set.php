<?php
function handler($event, $context)
{
    if ($event["isBase64Encoded"] === true) {
        $post_data = base64_decode($event['body']);
    } else {
        $post_data = $event['body'];
    }
    $decoded = json_decode($post_data, true);
    $response = json_encode($decoded, JSON_PRETTY_PRINT);
    $file = file_get_contents('set.json');
    $card = [];
    $cards = [];
    for ($i = 0; $i < strlen($file); $i++) {
        if (is_numeric($file[$i])) {
            $card[] = $file[$i];
        } else {
            if (count($card) === 4) {
                $cards[] = $card;
                $card = [];
            };
        };
    };

    $out = [];
    for ($i = 0; $i < count($cards) - 2; $i++) {
        for ($j = $i+1; $j < count($cards) - 1; $j++) {
            for ($k = $j+1; $k < count($cards); $k++) {
                $combo = [];
                for ($col = 0; $col < 4; $col++) {
                    $combo[] = [$cards[$i][$col], $cards[$j][$col], $cards[$k][$col]];
                };
                for ($c = 0; $c < count($combo); $c++) {
                    if ((array_sum($combo[$c]) == 6) || (array_sum($combo[$c]) == $combo[$c][0] * 3)) {
                        if ($combo[$c] == $combo[count($combo) - 1]) {
                            $out[] = "<br>Сет</br>";
                            foreach ([$i, $j, $k] as $value) {
                                $out[] = "count: " . $cards[$value][0] . " color: " . $cards[$value][1] . " shape: " . $cards[$value][2] . " fill: " . $cards[$value][3] . "<br>";
                            };
                            break;
                        } else {
                            continue;
                        };
                    } else {
                        break;
                    };
                };
            };
        };
    };
    $out[] = '<br>Если выше не перечислены варианты возможных сетов, значит их нет.';
    
    return [
        "statusCode" => 200,
        "body" => implode("\n", $out)
    ];
}
