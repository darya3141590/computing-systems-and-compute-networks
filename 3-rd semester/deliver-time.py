import matplotlib.pyplot as plt
import seaborn as sns
a = input("Введите название входного файла: ")
tup = []
with open(a) as f:
    for line in f:
        try:
            b = float(line.split("time=")[1].split(" ")[0])
            tup.append(b)
        except IndexError:
            continue
    data = dict((x, tup.count(x)) for x in set(tup))
    sorted_data = sorted(data.items(), key=lambda x: x[0])
    c = dict(sorted_data)
    h_axis = sorted(list(set(tup)))
    v_axis = []
    k = c.values()
    for i in k:
        v_axis.append(i)
    fig, ax = plt.subplots(figsize=(9, 6))
    sns.barplot(x=h_axis, y = v_axis, ax = ax, palette = 'plasma')
    ax.set_title('Нормальное распределение во времени доставки', fontsize = 18)
    ax.tick_params(labelsize=16, length=0)
    ax.yaxis.grid(linewidth=0.5, color='black')
    ax.set_axisbelow(True)
    plt.box(False)
plt.show()

