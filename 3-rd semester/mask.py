def bin_to_dec(digit, chislo_dec):
    if len(digit) != 0:
        for i in range(0, 8):
            chislo_dec = chislo_dec + int(digit[i]) * (2 ** (8 - i - 1))
    return chislo_dec
a, chislo_dec = int(input("Введите число от 0 до 32: ")), 0
if bin_to_dec(("1"*(a%8)+"0"*((32-a)%8)), chislo_dec) != 0:
    print("Маска подсети: ", ("255." * (a // 8) + str(bin_to_dec(("1"*(a%8)+"0"*((32-a)%8)), chislo_dec)) + "." + "0." * ((32 - a) // 8)).rstrip("."))
else:
    print("Маска подсети: ", ("255." * (a // 8) + "0." * ((32 - a) // 8)).rstrip("."))
