# adj = [[0] * n for _ in range(n)]
mas = [1,2, 3, 4, 4, 5]
N = len(set(mas))
adj = [[0] * N for _ in range(N)]
for i in range(0, len(mas), 2):
    adj[mas[i]-1][mas[i+1]-1] = adj[mas[i+1]-1][mas[i]-1] = 1
begin = list(set(mas))
begin.insert(0, "#")
adj.insert(0, begin)
for i in range(1, len(adj)):
    adj[i].insert(0, list(set(mas))[i-1])
for i in range(len(adj)):
    for j in range(len(adj[i])):
        print(adj[i][j], end=' ')
    print()